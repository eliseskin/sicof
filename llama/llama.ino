#include <LiquidCrystal.h>

#include <DHT_U.h>
#include <DHT.h>

#define DHTPIN 8     // Pin donde está conectado el sensor

#define DHTTYPE DHT11   // Descomentar si se usa el DHT 11
//#define DHTTYPE DHT22   // Sensor DHT22
int dig = 9;
int anal = A3;
int val;
float sensor;
//pines de pantalla LCD
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  // Indicamos la pantalla de 16X2
  lcd.begin(16, 2);
  // mover el cursor a la primera posision de la pantalla (0, 0)
  lcd.home();
  // Mover el cursor a la segunda linea (1) Primer columna (0)
  lcd.setCursor ( 0, 1 );
  //Abrimos la conexion al sensor de flama
  pinMode(dig, INPUT);
  pinMode(anal, INPUT);
  //iniciamos el monitor serie
  //Serial.println("Iniciando...");
  dht.begin();
}
void loop() {
  //delay(2000);
  //datos del sensor de llama
  sensor = analogRead(anal);
  //Serial.println(sensor);
  val = digitalRead(dig);     
  if (val >= 900) {
    Serial.println("Encendido");
  } else {
    Serial.println("Apagado");
  }

  float h = dht.readHumidity(); //Leemos la Humedad
  float t = dht.readTemperature(); //Leemos la temperatura en grados Celsius
  float f = dht.readTemperature(true); //Leemos la temperatura en grados Fahrenheit
  //--------Enviamos las lecturas por el puerto serial-------------
  //Serial.print("Humedad ");
  Serial.print(h);
  //Serial.print(",");
  Serial.print(t);
  Serial.println();
  //Serial.print(" %t");
  //Serial.print("Temperatura: ");
  //Serial.println(t);
  //Serial.print(" *C ");
  //Serial.print(f);
  //Serial.println(" *F");
  //datos por pantalla primera linea muestra nombre;
  lcd.home();
  lcd.print("Flama: ");
  lcd.print(sensor);
  lcd.setCursor(0, 1);
  lcd.print("H:");
  lcd.print(h);
  lcd.print("%t");
  lcd.print("T:");
  lcd.print(t);
  lcd.print("*C");
  delay(1000);
}
