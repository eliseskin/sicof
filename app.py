from flask import Flask, render_template, request, redirect, url_for, flash

import pyrebase
firebaseConfig = {
  "apiKey": "AIzaSyC4gwgnp_KEkB_yjBPYCIAybf74y40eY-8",
  "authDomain": "sisis-a8b28.firebaseapp.com",
  "databaseURL": "https://sisis-a8b28.firebaseio.com/",
  "storageBucket": "sisis-a8b28.appspot.com"
}
firebase = pyrebase.initialize_app(firebaseConfig)
db = firebase.database()

app = Flask(__name__)

@app.route("/")
def Index():
    data = db.child("dato").get()
    return render_template('index.html', dats = data)

if __name__ == "__main__":
    app.run(debug=True)