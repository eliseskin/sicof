"""Se declaran las librerias a utilizar"""
from ubidots import ApiClient
import serial 
import time    
import sys

""" Excepcion para verificar si el arduino esta conectado o no """       
try:  
    print("CONECTADO...")  
    arduino = serial.Serial('/dev/ttyUSB0', 9600, timeout=2.0)
    time.sleep(1)
    arduino.flush()
except:  
    print("FALLO LA CONEXION")

""" Excepcion para verificar si conecto correctamente con la API de ubidtos """ 
try:  
    print("CONECTADO API...")  
    api = ApiClient('BBFF-4dc3e790578baa4b4fcf6a2dde906487eba')
    humedad = api.get_variable('5d82a0e11d8472155112e1a6')
except:  
    print("FALLO LA CONEXION API")

""" Los siguientes contadores se utilizan para enviar el dato a ubitods de temperatura
recibir el dato que viene de ubidots, y realizar un reset al arduino, con el fin de
hacer una limpieza a bufer """ 
contador=0
contador1=0
contador2=0

""" Ciclo donde se desarrolla todo el programa """ 
while True:
	""" se lee el dato que llega del arduino """ 
	dato=arduino.readline()[:-7].strip()
    #dato2=arduino.readline(dato[0][5:]).strip()
	""" Contador que envia el dato cada 11 ciclos"""
	if contador == 11:	
		try:
			temp = humedad.save_value({'value':dato})
		except:
			print("NO ENVIO") 
		contador=0
    
	""" Contador que envia el dato cada 11 ciclos"""
	if contador1== 5:
		try:
			""" Se toma la variable de entrada de ubidots"""
			led = api.get_variable('5d82b3011d84722dd7310891')
			""" Se toma el valor la variable de entrada"""
			new_value = led.get_values(1)
			""" Se convierte este valor de tipo INFO a LISTA"""
			lista = str(new_value)
			""" Se toma de la LISTA el valor 177 el cual corresponde a 1 o 0 """
			valor=lista[177]
			""" Se compara el valor si es 1 o 0, y se envia este mismo"""
			if valor=='1':
				arduino.write(valor)
			else:
				arduino.write(valor)
		except:
			print("NO ENVIO")
		contador1=0
	""" Contador que reset el arduino cada 100 ciclos"""	
	if contador2 == 100:	
		arduino.close()
		arduino = serial.Serial('/dev/ttyUSB0', 9600, timeout=0.5)
		contador2=0
    
	""" Imprimir el dato que llega del arduino y hacer la suma a cada contador"""
	print(dato)
	contador=contador+ 1
	contador1=contador1+ 1 
	contador2=contador2+ 1